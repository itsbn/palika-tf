resource "aws_key_pair" "palika_kp" {
  key_name   = "palika-kp"
  public_key = file(var.PATH_TO_PUBLIC_KEY)
}
