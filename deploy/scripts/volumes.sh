#!/bin/bash

#
#   Passed variables from calling script...
#       DEVICE
#
set -ex

vgchange -ay

DEVICE_FS=`blkid -o value -s TYPE ${DEVICE} || echo ""`

#   Device file system does not exist...
if [ "`echo -n $DEVICE_FS`" == "" ] ; then
  # wait for the device to be attached
  DEVICENAME=`echo "${DEVICE}" | awk -F '/' '{print $3}'`
  DEVICEEXISTS=''
  while [[ -z $DEVICEEXISTS ]]; do
    echo "checking $DEVICENAME"
    DEVICEEXISTS=`lsblk |grep "$DEVICENAME" |wc -l`
    if [[ $DEVICEEXISTS != "1" ]]; then
      sleep 15
    fi
  done

  # Execute utilities to create file system and volume data...
  pvcreate ${DEVICE}
  vgcreate data ${DEVICE}
  lvcreate --name vol_appdata -l 100%FREE data
  mkfs.ext4 /dev/data/vol_appdata

fi

#   Create directory if it does not exist...
mkdir -p /data
echo '/dev/data/vol_appdata /data ext4 defaults 0 0' >> /etc/fstab
mount /data

# install docker
curl https://get.docker.com | bash
