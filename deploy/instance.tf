###
#   EC2 Moodule and it's components
#
#       Revision:   Initial TF Script ... V0.13
#       Date:       08/19/2020
#       Editor:     @mj@d
#
###
resource "aws_instance" "palika_dev_core" {
  ami           = var.ubuntu-amis[var.AWS_REGION]
  instance_type = var.APP_SERVER_INSTANCE_TYPE

  # the VPC subnet
  subnet_id = aws_subnet.palika-public-1.id

  # the security group
  vpc_security_group_ids = [aws_security_group.allow-ssh.id]

  # the public SSH key
  key_name = aws_key_pair.palika_kp.key_name

  # user data
  user_data = data.template_cloudinit_config.cloudinit-example.rendered

    tags = {
      Name = "${var.PROJECT_NAME}Core0820"
    }

}
