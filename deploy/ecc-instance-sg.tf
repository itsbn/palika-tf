###
#   EC2 Security Group Moodule and it's components
#
#       Revision:   Initial TF Script ... V0.13
#       Date:       08/19/2020
#       Editor:     @mj@d
#
###
resource "aws_security_group" "allow-ssh" {
  vpc_id      = aws_vpc.palika.id
  name        = "allow-ssh"
  description = "Security group that allows ssh and all egress traffic to/from an instance"
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.SSH_CIDR_WEB_SERVER]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.SSH_CIDR_WEB_SERVER]
  }

    tags = {
      Name = "${var.PROJECT_NAME} - Allow SSH Security Group"
    }
}
