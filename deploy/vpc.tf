###
#   VPC Moodule and it's components
#
#       Revision:   Initial Script
#       Date:       08/15/2020
#       Editor:     @mj@d
#
###



# Custom VPC
resource "aws_vpc" "palika" {
  cidr_block           = var.VPC_CIDR
  instance_tenancy     = "default"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"

  tags = {
    Name = "${var.PROJECT_NAME}-vpc"
  }
}

# Subnets
resource "aws_subnet" "palika-public-1" {
  vpc_id                  = aws_vpc.palika.id
  cidr_block              = var.VPC_PUBSUBNET1_CIDR
  map_public_ip_on_launch = "true"
  availability_zone       = "${var.AWS_REGION}a"

  tags = {
    Name = "${var.PROJECT_NAME}-public-subnet-1"
  }
}

resource "aws_subnet" "palika-public-2" {
  vpc_id                  = aws_vpc.palika.id
  cidr_block              = var.VPC_PUBSUBNET2_CIDR
  map_public_ip_on_launch = "true"
  availability_zone       = "${var.AWS_REGION}b"

  tags = {
    Name = "${var.PROJECT_NAME}-public-subnet-2"
  }
}

resource "aws_subnet" "palika-public-3" {
  vpc_id                  = aws_vpc.palika.id
  cidr_block              = var.VPC_PUBSUBNET3_CIDR
  map_public_ip_on_launch = "true"
  availability_zone       = "${var.AWS_REGION}c"

  tags = {
    Name = "${var.PROJECT_NAME}-public-subnet-3"
  }
}

resource "aws_subnet" "palika-private-1" {
  vpc_id                  = aws_vpc.palika.id
  cidr_block              = var.VPC_PVTSUBNET1_CIDR
  map_public_ip_on_launch = "false"
  availability_zone       = "${var.AWS_REGION}a"

  tags = {
    Name = "${var.PROJECT_NAME}-private-subnet-1"
  }
}

resource "aws_subnet" "palika-private-2" {
  vpc_id                  = aws_vpc.palika.id
  cidr_block              = var.VPC_PVTSUBNET2_CIDR
  map_public_ip_on_launch = "false"
  availability_zone       = "${var.AWS_REGION}b"

  tags = {
    Name = "${var.PROJECT_NAME}-private-subnet-2"
  }
}

resource "aws_subnet" "palika-private-3" {
  vpc_id                  = aws_vpc.palika.id
  cidr_block              = var.VPC_PVTSUBNET3_CIDR
  map_public_ip_on_launch = "false"
  availability_zone       = "${var.AWS_REGION}c"

  tags = {
    Name = "${var.PROJECT_NAME}-private-subnet-3"
  }
}

# Internet GW
resource "aws_internet_gateway" "palika-igw" {
  vpc_id = aws_vpc.palika.id

  tags = {
    Name = "${var.PROJECT_NAME}-igw"
  }
}

# route tables
resource "aws_route_table" "palika-public-rtbl" {
  vpc_id = aws_vpc.palika.id
  route {
    cidr_block = var.SSH_CIDR_APP_SERVER
    gateway_id = aws_internet_gateway.palika-igw.id
  }

  tags = {
    Name = "${var.PROJECT_NAME}-public-rtbl"
  }
}

# route associations public
resource "aws_route_table_association" "palika-public-1-a" {
  subnet_id      = aws_subnet.palika-public-1.id
  route_table_id = aws_route_table.palika-public-rtbl.id
}

resource "aws_route_table_association" "palika-public-2-a" {
  subnet_id      = aws_subnet.palika-public-2.id
  route_table_id = aws_route_table.palika-public-rtbl.id
}

resource "aws_route_table_association" "palika-public-3-a" {
  subnet_id      = aws_subnet.palika-public-3.id
  route_table_id = aws_route_table.palika-public-rtbl.id
}
