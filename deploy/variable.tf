#   Default Variables
#       Revision:   Initial Script
#       Date:       08/13/2020
#       Editor:     @mj@d
#

# Project wide variable
variable "PROJECT_NAME" {}

#   Varibles for the Providers
#   (use aws-vault for access)

variable "AWS_REGION" {}

variable "region_alias" {}
#variable "project" {}

#   ubuntu 20 LTS (http://cloud-images-ubuntu.com/locator)
variable "ubuntu-amis" {
  type = map(string)
  default = {
    us-east-1 = "ami-05cf2c352da0bfb2e"
    us-east-2 = "ami-0e45766c39d6d6e73"
    us-west-1 = "ami-0b10e1018d4058364"

  }
}

##
#   Route 53 variables
##
variable "rt53-hosted-zone" {}
variable "rt53-reg-domain" {}
variable "rt53-dns-alias" {}



variable "PATH_TO_PRIVATE_KEY" {}
variable "PATH_TO_PUBLIC_KEY" {}

variable "INSTANCE_DEVICE_NAME" {}

# RDS variables
variable "RDS_CIDR" {}

variable "DB_INSTANCE_CLASS" {}
variable "RDS_ENGINE" {}
variable "ENGINE_VERSION" {}
variable "BACKUP_RETENTION_PERIOD" {}
variable "PUBLICLY_ACCESSIBLE" {}
variable "RDS_USERNAME" {}
variable "RDS_PASSWORD" {}
variable "RDS_ALLOCATED_STORAGE" {}

# Ec2 /Autoscaling Variables
variable "SSH_CIDR_WEB_SERVER" {}

variable "SSH_CIDR_APP_SERVER" {}
variable "WEB_SERVER_INSTANCE_TYPE" {}
variable "APP_SERVER_INSTANCE_TYPE" {}
variable "USER_DATA_FOR_WEBSERVER" {}
variable "USER_DATA_FOR_APPSERVER" {}
variable "PEM_FILE_APPSERVERS" {}
variable "PEM_FILE_WEBSERVERS" {}

# VPC Variables
variable "VPC_CIDR" {}

variable "VPC_PUBSUBNET1_CIDR" {}
variable "VPC_PUBSUBNET2_CIDR" {}
variable "VPC_PUBSUBNET3_CIDR" {}

variable "VPC_PVTSUBNET1_CIDR" {}
variable "VPC_PVTSUBNET2_CIDR" {}
variable "VPC_PVTSUBNET3_CIDR" {}
