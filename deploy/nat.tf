###
#   Gateways Moodule and it's components
#
#       Revision:   Initial TF Script ... V0.13
#       Date:       08/19/2020
#       Editor:     @mj@d
#
###
# nat gw
resource "aws_eip" "nat" {
  vpc = true
}

resource "aws_nat_gateway" "nat-gw" {
  allocation_id = aws_eip.nat.id
  subnet_id     = aws_subnet.palika-public-1.id
  depends_on    = [aws_internet_gateway.palika-igw]

  tags = {
    Name = "${var.PROJECT_NAME} - NA Gateway PubSubnet 1"
  }
}

# VPC setup for NAT
resource "aws_route_table" "palika-private" {
  vpc_id = aws_vpc.palika.id
  route {
    cidr_block     = var.SSH_CIDR_APP_SERVER
    nat_gateway_id = aws_nat_gateway.nat-gw.id
  }

  tags = {
    Name = "${var.PROJECT_NAME} - NA Gateway Private Route Table "
  }
}

# route associations private
resource "aws_route_table_association" "palika-private-1-a" {
  subnet_id      = aws_subnet.palika-private-1.id
  route_table_id = aws_route_table.palika-private.id
}

resource "aws_route_table_association" "palika-private-2-a" {
  subnet_id      = aws_subnet.palika-private-2.id
  route_table_id = aws_route_table.palika-private.id
}

resource "aws_route_table_association" "palika-private-3-a" {
  subnet_id      = aws_subnet.palika-private-3.id
  route_table_id = aws_route_table.palika-private.id
}
