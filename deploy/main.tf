#   Terraform backend state management in S3
terraform {
  backend "s3" {
    bucket         = "coreapp-config-tf"
    key            = "coreapp-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "coreapp-tf-state-lock"
  }
}

#   AWS project specifications and version
provider "aws" {
  region  = var.AWS_REGION
  version = "~> 3.0"
}
