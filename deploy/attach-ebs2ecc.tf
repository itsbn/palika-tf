###
#   EBS Module to attach data volume to an instance
#       Revision:   Initial Script
#       Date:       08/20/2020
#       Editor:     @mj@d
#
###
resource "aws_ebs_volume" "ebs_vol_appext" {
  availability_zone = "${var.AWS_REGION}a"
  size              = 20
  type              = "gp2"
  tags = {
    Name = "${var.PROJECT_NAME} Extended volume data for application"
  }
}

resource "aws_volume_attachment" "ebs-volume-1-attachment" {
  device_name = var.INSTANCE_DEVICE_NAME
  volume_id   = aws_ebs_volume.ebs_vol_appext.id
  instance_id = aws_instance.palika_dev_core.id
}
