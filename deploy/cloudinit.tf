###
#   Post Provisioning Module to execute scripts in EC2
#   instance after the launch to mount data volume after an
#   instance is attached with EBS volume for immutable structure
#
#       Revision:   Initial Script
#       Date:       08/20/2020
#       Editor:     @mj@d
#
###
data "template_file" "init-script" {
  template = file("deploy/scripts/init.cfg")

  # Variables to pass to the script...
  vars = {
    REGION = var.AWS_REGION
  }
}

data "template_file" "shell-script" {
  template = file("deploy/scripts/volumes.sh")

  # Variables to pass to the script...
  vars = {
    DEVICE = var.INSTANCE_DEVICE_NAME
  }
}

data "template_cloudinit_config" "cloudinit-example" {
  gzip          = false
  base64_encode = false

  part {
    filename     = "init.cfg"
    content_type = "text/cloud-config"
    content      = data.template_file.init-script.rendered
  }

  part {
    content_type = "text/x-shellscript"
    content      = data.template_file.shell-script.rendered
  }
}
