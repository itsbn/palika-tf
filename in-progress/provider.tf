provider "aws" {
  project = "var.aws_project"
  alias   = "var.region_alias"
  region  = "var.aws_region"
}
