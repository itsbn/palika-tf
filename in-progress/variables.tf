#   Default Variables
#       Revision:   Initial Script
#       Date:       08/13/2020
#       Editor:     @mj@d
#
variable "_location" {
  default = "Chicago"
}

variable "_buildenv" {
  default = "coredev"
}

variable "region_alias" {
  default = "eastnv"
}

variable "aws_region" {
  default = "us-east-1"
}

variable "vpc_cidr" {
  default = "10.0.0.0/16"
}

variable "subnets_cidr" {
  type    = list
  default = ["10.0.1.0/16", "10.0.2.0/16"]
}

variable "azs" {
  type    = list
  default = ["us-east-1c", "us-east-1d"]
}
